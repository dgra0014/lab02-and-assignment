// Fill out your copyright notice in the Description page of Project Settings.


#include "ShootableActor.h"

// Sets default values
AShootableActor::AShootableActor()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	RootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("RootComponent"));
	mVisibleComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("VisibleComponent"));
	mVisibleComponent->SetupAttachment(RootComponent);
}

// Called when the game starts or when spawned
void AShootableActor::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AShootableActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	int shouldChangeDir = FMath::RandRange(0, 10);
	if (shouldChangeDir > 9)
	{
		randomMovementDir = FMath::RandRange(0, 2);
	}

	if (randomMovementDir == 0)
		SetActorLocation(GetActorLocation() + (FVector(0, -speed, 0) * DeltaTime));
	else if (randomMovementDir == 2)
		SetActorLocation(GetActorLocation() + (FVector(0, speed, 0) * DeltaTime));

	prevIsJumping = isJumping;

	if (!isJumping && FMath::RandRange(0, 15) > 14)
		isJumping = true;
	else if (isJumping && FMath::RandRange(0, 10) > 9)
		isJumping = false;

	if(isJumping && !prevIsJumping)
		SetActorLocation(GetActorLocation() + (FVector(0, 0, 100)));
	if (!isJumping && prevIsJumping)
		SetActorLocation(GetActorLocation() + (FVector(0, 0, -100)));
}

void AShootableActor::OnBulletHit()
{
	Destroy();
}