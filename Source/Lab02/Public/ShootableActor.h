// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Math/UnrealMathUtility.h"
#include "ShootableActor.generated.h"

UCLASS()
class LAB02_API AShootableActor : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AShootableActor();

	void OnBulletHit();

	UPROPERTY(EditAnywhere)
		UStaticMeshComponent* mVisibleComponent;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

private:
	int randomMovementDir = 2;
	float speed = 500.0f;
	bool isJumping = false, prevIsJumping = false;
};
