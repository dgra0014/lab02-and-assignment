// Fill out your copyright notice in the Description page of Project Settings.


#include "CharacterPawn.h"

// Sets default values
ACharacterPawn::ACharacterPawn()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	AutoPossessPlayer = EAutoReceiveInput::Player0;
	RootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("RootComponent"));
	mCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("Camera"));
	mCamera->SetupAttachment(RootComponent);
}

// Called when the game starts or when spawned
void ACharacterPawn::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ACharacterPawn::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (!mMovementInput.IsZero())
	{
		mMovementInput.Normalize();
		SetActorLocation(GetActorLocation() + (mMovementInput * speed * DeltaTime));
	}
}

// Called to bind functionality to input
void ACharacterPawn::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	PlayerInputComponent->BindAxis("MoveX", this, &ACharacterPawn::MoveForward);
	PlayerInputComponent->BindAxis("MoveY", this, &ACharacterPawn::MoveRight);
	PlayerInputComponent->BindAction("Shoot", IE_Pressed, this, &ACharacterPawn::Shoot);
}

void ACharacterPawn::MoveForward(float axisValue) { mMovementInput.X = axisValue; }
void ACharacterPawn::MoveRight(float axisValue) { mMovementInput.Y = axisValue; }

void ACharacterPawn::Shoot() 
{
	UE_LOG(LogTemp, Warning, TEXT("BANG"));

	FHitResult linetraceResult;
	FVector traceStart = GetActorLocation();
	traceStart.X += 200;
	FVector traceEnd = (GetActorForwardVector() * 7500.0f) + traceStart;
	FCollisionQueryParams params;

	bool isHit = GetWorld()->LineTraceSingleByChannel(linetraceResult, traceStart, traceEnd, ECC_WorldStatic, params);

	if (isHit)
	{
		AShootableActor* target = Cast<AShootableActor>(linetraceResult.GetActor());
		if (target)
		{
			UE_LOG(LogTemp, Warning, TEXT("HIT Shootable Actor!"));
			target->OnBulletHit();
		}
	}
}