// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "Lab02GameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class LAB02_API ALab02GameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
